#!/bin/bash
##
##   -   '-'   -
##  / `.=) (=.` \
## /.-.-.\ /.-.-.\
##       'v'
##
## RaspPiBat recorder 2015
## Raspberry Pi Model A+/B+ with Cirrus Logic 192kHz SoundBoard // Version 20150727

#PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

## Recorder ID
export FileNamePrefix="rpibat01"

## Timestamps
## Precise timestamp
DateTime() {
date +%Y%m%d_%H%M%S
}
export TimeStamp=`DateTime`

## Simplified timestamps
DateTimeSimp() {
date +%Y%m%d_%H%M
}
export TimeStampSimp=`DateTimeSimp`

DateSimp() {
date +%Y%m%d
}
export DateStampSimp=`DateSimp`

## Data/Temp/Log directories location
if sudo fdisk -l | grep -q /dev/sda1 ; then
		echo "$TimeStamp: usb disk detected"
		sudo mkdir /media/usbdisk;
## If usb disk filesystem is ExFAT or NTFS (generally used for usb key over 32Go). You need to install specific librairies. 
## You can install them with this command "sudo apt-get install exfat-fuse ntfsprogs"
		sudo mount -t auto /dev/sda1 /media/usbdisk;
fi

if mount | grep -q /media/usbdisk ; then
		echo "$TimeStamp : usb disk connected";
		mkdir "/media/usbdisk/rpibat" ;
		export RpiBatDir='/media/usbdisk/rpibat';
		mkdir "/media/usbdisk/rpibat/data" ;
		export DataDir="/media/usbdisk/rpibat/data/";
		mkdir "/media/usbdisk/rpibat/temp" ;
		export TempDir="/media/usbdisk/rpibat/temp/";
		mkdir "/media/usbdisk/rpibat/log" ;
		export LogDir="/media/usbdisk/rpibat/log/";
		echo "$TimeStamp : Data/Temp/Log folders created on USB disk (folder: ../media/usbdisk/rpibat)";
	else
		echo "$TimeStamp : no usb disk";
		mkdir "/rpibat/$DateStampSimp_$FileNamePrefix" ;
		export RpiBatDir='/rpibat/';
		mkdir "/rpibat/data" ;
		export DataDir="/rpibat/data/";
		mkdir "/rpibat/temp" ;
		export TempDir="/rpibat/temp/";
		mkdir "/rpibat/log" ;
		export LogDir="/rpibat/log/";
		echo "$TimeStamp : Data/Temp/Log folders created on SDcard (folder: /rpibat)";
fi

export LogFile="$LogDir$FileNamePrefix-$TimeStamp.log"
echo "$TimeStamp : Start recording" >> "/$LogDir/startstop.log"
echo "$TimeStamp : New session" >> "/$LogDir/recordings_$TimeStampSimp.log"

##  Launch of the recording scripts
/rpibat/screenrecordingsession.sh $FileNamePrefix 1>> "/$LogDir/recordings_$TimeStampSimp.log" 2>>&1 &

exit 0